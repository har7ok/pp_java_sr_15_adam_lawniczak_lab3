// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************


import java.text.NumberFormat;

public class Koszyk
{

    private static int iloscRzeczy;       // ilosc rzeczy w koszyku
    private static double cenaCalkowita;  // calkowita cena przedmiotow w koszyku

    // -----------------------------------------------------------
    // tworzy pusty koszyk
    // -----------------------------------------------------------
    public Koszyk()
    {
	iloscRzeczy = 0;
	cenaCalkowita = 0.0;
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(Item item)
    {
    	
    	Zakupy.koszyk.add(item);
    	iloscRzeczy+=1;
    	cenaCalkowita += item.getCena() * item.getIlosc();
    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();
	String zawartosc = "\nKoszyk\n";
	zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\tSuma\n";
	
	for (int i = 0; i < iloscRzeczy; i++)
	{
		zawartosc += (Zakupy.koszyk.get(i)).toString() + "\n";
	}
	    

	zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
	zawartosc += "\n";

	return zawartosc;
    }

}

