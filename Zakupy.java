// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiotów i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************



import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;

import cs1.Keyboard;

public class Zakupy
{
	
	
	
	static public final ArrayList<Item> koszyk = new ArrayList<Item>();
    public static void main (String[] args)
    {
    	
    
    Item rzecz;
	String nazwaRzeczy;
	double cenaRzeczy;
	int ilosc;

	String kontynuujZakupy = "t";
	
	Koszyk Basket = new Koszyk();
	
	
	try {
		
		
		  
		
		 
		File fXmlFile = new File("basket1.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
	 
		doc.getDocumentElement().normalize();
	 
	 
		NodeList nList = doc.getElementsByTagName("item");
	 
		for (int temp = 0; temp < nList.getLength(); temp++) {
	 
			Node nNode = nList.item(temp);
	 
	 
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
				Element eElement = (Element) nNode;
	 
				nazwaRzeczy = eElement.getElementsByTagName("name").item(0).getTextContent();
				cenaRzeczy = Double.parseDouble(eElement.getElementsByTagName("price").item(0).getTextContent());
				ilosc = Integer.parseInt(eElement.getElementsByTagName("count").item(0).getTextContent());
				
				rzecz = new Item(nazwaRzeczy, cenaRzeczy, ilosc);
				Basket.dodajDoKoszyka(rzecz);
			}
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	
	System.out.println(Basket);
	System.out.print ("Kontynuowac zakupy (t/n)? ");
	kontynuujZakupy = Keyboard.readString();
	
if (kontynuujZakupy.equals("t"))
	do 
	    {
		
		
		System.out.print ("Podaj nazwe rzeczy: "); 
		nazwaRzeczy = Keyboard.readString();

		System.out.print ("Podaj cene jednostkowa: ");
		cenaRzeczy = Keyboard.readDouble();

		System.out.print ("Podaj ilosc: ");
		ilosc = Keyboard.readInt();
		
		
		rzecz = new Item(nazwaRzeczy, cenaRzeczy, ilosc);
		Basket.dodajDoKoszyka(rzecz);

		
		System.out.println(Basket);

		System.out.print ("Kontynuowac zakupy (t/n)? ");
		kontynuujZakupy = Keyboard.readString();
	    }
	while (kontynuujZakupy.equals("t"));

    }
}

